
// in this line of code we are getting the document in the HTML element with the id txt-first-name
console.log(document);

const txtFirstname = document.querySelector("#txt-first-name")

const txtLastName = document.querySelector("#txt-last-name")

const spanFullname = document.querySelector("#span-full-name")


txtFirstname.addEventListener('keyup', (event) => {
	spanFullname.innerHTML = txtFirstname.value
});

txtLastName.addEventListener('keyup', (event) => {
	spanFullname.innerHTML = `${txtFirstname.value} ${txtLastName.value}`
});

