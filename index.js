
// in this line of code we are getting the document in the HTML element with the id txt-first-name
console.log(document);

const txtFirstname = document.querySelector("#txt-first-name")

const spanFullname = document.querySelector("#span-full-name")
// alternative way of targeting an element
// document.getElementById("txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

txtFirstname.addEventListener('keyup', (event) => {
	spanFullname.innerHTML = txtFirstname.value
})

txtFirstname.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value)
})

